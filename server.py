import socket
IP="66.81.170.141"
PORT=21080
MAX_OPEN_REQUESTS=5
s_ser=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
hostname=IP
try:
    s_ser.bind((IP,PORT))
    s_ser.listen(MAX_OPEN_REQUESTS)
    while True:
        print("Waiting for connections at %s %i" % (hostname, PORT))
        (clientsocket, address) = s_ser.accept()
        mensaje="¿Qué deseas decir"
        mensaje_by=str.encode(mensaje)
        clientsocket.send(mensaje_by)
        print(mensaje)
except:
    print("Problemas using port %i. Do you have permission?" % PORT)